<?php
  echo '<h3>Task 4: Вычислить факториал числа n</h3>';

  function taskFour($n) {
    $taskFourResult = '';
    $factorial = 1;

    if ($n < 0) {
      return 'Факториала отрицательного числа не существует!';
    } else if ($n === 0 || $n === 1) {
      $taskFourResult = "$n! = 1";
    } else if ($n > 1) {
      for ($i = 2; $i <= $n; $i++) {
        $factorial *= $i;
      }
        $taskFourResult = "$n! = $factorial";
    }

    return $taskFourResult;
  };

  echo '<h4>Передаём 5:</h4>';
  echo taskFour(5);

