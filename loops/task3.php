<?php
  echo '<h3>Task 3: Найти корень натурального числа с точностью до целого</h3>';

  function taskThree($number) {
    if ($number < 0) {
      return 'Ошибка: введите натуральное число!';
    }

    $i = 1;

    while($i * $i <= $number) {
      $i++;
    }

    $sqrtRoot = $i - 1;

    return "Корень $number, найден методом последовательного подбора = $sqrtRoot";
  };

  echo '<h4>Передаём 156:</h4>';
  echo taskThree(156);

  function taskThreeBinary($number) {
    $left = 0;
    $right = $number;
    $sqrtRootBinary = 0;

    if ($number < 0) {
      return 'Ошибка: введите натуральное число!';
    } else {
      while ($right > 0) {
        $middle = $left + ($right - $left) / 2;
        if (round($middle * $middle, 0) > $number) {
          $right = $middle;
        } else if (round($middle * $middle, 0) < $number) {
          $left = $middle;
        } else if (round($middle * $middle, 0) == $number) {
          $sqrtRootBinary = round($middle, 0);
          break;
        }
      }
    }

    return "Корень $number, найден методом бинарного поиска = $sqrtRootBinary";
  };

  echo '<h4>Передаём 156:</h4>';
  echo taskThreeBinary(156);