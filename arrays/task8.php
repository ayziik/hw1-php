<?php
  echo '<h3>Task 8: Поменять местами первую и вторую половину массива</h3>';

  function taskEight($someArr) {
    if (count($someArr) % 2 == 0) {
      $half = count($someArr) / 2;

      for ($i = 0; $i < $half; $i++) {
        $currentElement = $someArr[$i];
        $someArr[$i] = $someArr[$half + $i];
        $someArr[$half + $i] = $currentElement;
      }
    } else {
      $median = (count($someArr) - 1) / 2;

      for ($i = 0; $i < $median; $i++) {
        $currentElement = $someArr[$i];
        $someArr[$i] = $someArr[$median + $i + 1];
        $someArr[$median + $i + 1] = $currentElement;
      }
    }

    return $someArr;
  };

  echo '<h4>Передаём [4, 4, 2, 2]:</h4>';
  print_r(taskEight([4, 4, 2, 2]));

