<?php
  echo '<h3>Task 3: Найти суммы только положительных из трех чисел</h3>';

  function taskThree($a, $b, $c) {
    $taskThreeResult = 0;

    if ($a > 0) {
      $taskThreeResult += $a;
    }
    if ($b > 0) {
      $taskThreeResult += $b;
    }
    if ($c > 0) {
      $taskThreeResult += $c;
    }
    if ($a < 0 && $b < 0 && $c < 0) {
      $taskThreeResult = 'Ошибка: все числа отрицательные!';
    }

    return $taskThreeResult;
  };

  echo '<h4>Передаём a = 2, b = 6, c = -8:</h4>';
  echo taskThree(2, 6, -8);

  echo '<h4>Передаём a = 3, b = -6, c = -8:</h4>';
  echo taskThree(3, -6, -8);