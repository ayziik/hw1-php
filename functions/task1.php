<?php
  echo '<h3>Task 1: Получить строковое название дня недели по номеру дня</h3>';

  function taskOne($day) {
    $days = [
      'Понедельник',
      'Вторник',
      'Среда',
      'Четверг',
      'Пятница',
      'Суббота',
      'Воскресенье'
    ];

    $taskOneResult = $days[$day - 1];

    if($day < 1 || $day > 7) {
      $taskOneResult = "Ошибка: введите чило от 1 до 7!";
    }

    return $taskOneResult;
  };

  echo '<h4>Передаём 3:</h4>';
  echo taskOne(3);


