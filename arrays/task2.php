<?php
  echo '<h3>Task 2: Найти максимальный элемент массива</h3>';

  function taskTwo($someArr) {
    return max($someArr);
  };

  echo '<h4>Передаём [2, -3, -1, 3]:</h4>';
  echo taskTwo([2, -3, -1, 3]);