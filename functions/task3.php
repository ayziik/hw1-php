<?php
  echo '<h3>Task 3 => Вводим строку, которая содержит число, написанное прописью (0-999)+ Получить само число</h3>';

  function taskThree($string) {
    $dg = array(
      'ноль' => 0,
      'один'=> 1,
      'два'=> 2,
      'три'=> 3,
      'четыре'=> 4,
      'пять'=> 5,
      'шесть'=> 6,
      'семь'=> 7,
      'восемь' => 8,
      'девять'=> 9
    );

    $tn = array(
      'десять' => 10,
      'одиннадцать' => 11,
      'двенадцать' => 12,
      'тринадцать' => 13,
      'четырнадцать' => 14,
      'пятнадцать' => 15,
      'шестнадцать' => 16,
      'семнадцать' => 17,
      'восемнадцать' => 18,
      'девятнадцать' => 19
    );

    $tw = array(
      'двадцать' => 20,
      'тридцать' => 30,
      'сорок' => 40,
      'пятьдесят' => 50,
      'шестьдесят' => 60,
      'семьдесят' => 70,
      'восемьдесят' => 80,
      'девяносто' => 90
    );

    $hd = array(
      'сто' => 100,
      'двести' => 200,
      'триста' => 300,
      'четыреста' => 400,
      'пятьсот' => 500,
      'шестьсот' => 600,
      'семьсот' => 700,
      'восемьсот' => 800,
      'девятьсот' => 900
    );

    $taskThreeResult = 0;
    $stringToArr = explode(' ', $string);
    $indexZero = $stringToArr[0];
    $indexOne = $stringToArr[1];
    $indexTwo = $stringToArr[2];

    if(count($stringToArr) == 1) {
      if(array_key_exists($indexZero, $dg)) {
        $taskThreeResult = $dg[$indexZero];
      } else if(array_key_exists($indexZero, $tn)) {
        $taskThreeResult = $tn[$indexZero];
      } else if(array_key_exists($indexZero, $tw)) {
        $taskThreeResult = $tw[$indexZero];
      } else if(array_key_exists($indexZero, $hd)) {
        $taskThreeResult = $hd[$indexZero];
      } else {
        $taskThreeResult = 'Ошибка: введите корректное значение!';
      }
    }

    if(count($stringToArr) == 2) {
      if(array_key_exists($indexZero, $tw) && array_key_exists($indexOne, $dg)) {
        $taskThreeResult = $tw[$indexZero] + $dg[$indexOne];
      } else if(array_key_exists($indexZero, $hd) && array_key_exists($indexOne, $dg)) {
        $taskThreeResult = $hd[$indexZero] + $dg[$indexOne];
      } else if(array_key_exists($indexZero, $hd) && array_key_exists($indexOne, $tn)) {
        $taskThreeResult = $hd[$indexZero] + $tn[$indexOne];
      } else if(array_key_exists($indexZero, $hd) && array_key_exists($indexOne, $tw)) {
        $taskThreeResult = $hd[$indexZero] + $tw[$indexOne];
      } else {
        $taskThreeResult = 'Ошибка: введите корректное значение!';
      }
    }

    if(count($stringToArr) == 3) {
      if(array_key_exists($indexZero, $hd) && array_key_exists($indexOne, $tw)
        && array_key_exists($indexTwo, $dg)) {
        $taskThreeResult = $hd[$indexZero] + $tw[$indexOne] + $dg[$indexTwo];
      } else {
        $taskThreeResult = 'Ошибка: введите корректное значение!';
      }
    }

    return $taskThreeResult;
  };

  echo '<h4>Передаём "семьсот пятьдесят два":</h4>';
  echo taskThree('семьсот пятьдесят два');


