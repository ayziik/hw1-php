<?php
  echo '<h3>Task 6: Вывести число, которое является зеркальным отображением последовательности цифр заданного числа</h3>';

  function taskSix($number) {
    $stringNumber = (string) $number;
    $taskSixResult = '';

    for ($i = strlen($stringNumber) - 1; $i >= 0; $i--) {
        $taskSixResult .= $stringNumber[$i];
    }

    return (int) $taskSixResult;
  };

echo '<h4>Передаём 5319:</h4>';
echo taskSix(5319);


