<?php
  echo '<h3>Task 2: Вводим число (0-999), получаем строку с прописью числа</h3>';

  function taskTwo($number) {
    $dg = [
      'ноль',
      'один',
      'два',
      'три',
      'четыре',
      'пять',
      'шесть',
      'семь',
      'восемь',
      'девять'
    ];

    $tn = [
      'десять',
      'одиннадцать',
      'двенадцать',
      'тринадцать',
      'четырнадцать',
      'пятнадцать',
      'шестнадцать',
      'семнадцать',
      'восемнадцать',
      'девятнадцать'
    ];

    $tw = [
      'двадцать',
      'тридцать',
      'сорок',
      'пятьдесят',
      'шестьдесят',
      'семьдесят',
      'восемьдесят',
      'девяносто'
    ];

    $hd = [
      'сто',
      'двести',
      'триста',
      'четыреста',
      'пятьсот',
      'шестьсот',
      'семьсот',
      'восемьсот',
      'девятьсот'
    ];

    $taskTwoResult = '';
    $numberToString = (string) $number;

    if($number < 0 || $number > 999) {
      $taskTwoResult = 'Ошибка: введите число от 0 до 999';
    }

    if(strlen($numberToString) < 2) {
      $taskTwoResult = $dg[$number];
    }

    if(strlen($numberToString) == 2) {
      if($number >= 10 && $number < 20) {
        $tnIndex =  (int) $numberToString[1];

        $taskTwoResult = $tn[$tnIndex];
      } else if ($number >= 20 && $number <= 99) {
        if($number % 10 == 0) {
          $twIndex = (int) $numberToString[0] - 2;

          $taskTwoResult = $tw[$twIndex];
        } else if($number % 10 > 0) {
          $twIndex = (int) $numberToString[0] - 2;
          $dgIndex = (int) $numberToString[1];

          $taskTwoResult = $tw[$twIndex] . ' '. $dg[$dgIndex];
        }
      }
    }

    if(strlen($numberToString) == 3) {
      if($number % 100 == 0) {
        $hdIndex = (int) $numberToString[0] - 1;

        $taskTwoResult = $hd[$hdIndex];
      } else if($number % 100 > 0 && $number % 100 < 10) {
        $hdIndex = (int) $numberToString[0] - 1;
        $dgIndex = (int) $numberToString[2];

        $taskTwoResult = $hd[$hdIndex] . ' ' . $dg[$dgIndex];
        } else if($number % 100 >= 10 && $number % 100 < 20) {
        $hdIndex = (int) $numberToString[0] - 1;
        $tnIndex = (int) $numberToString[2];

        $taskTwoResult = $hd[$hdIndex] . ' ' . $tn[$tnIndex];
        } else if($number % 100 >= 20) {
        if(($number % 100) % 10 == 0) {
          $hdIndex = (int) $numberToString[0] - 1;
          $twIndex = (int) $numberToString[1] - 2;

          $taskTwoResult = $hd[$hdIndex] . ' ' . $tw[$twIndex];
        } else if(($number % 100) % 10 > 0) {
          $hdIndex = (int) $numberToString[0] - 1;
          $twIndex = (int) $numberToString[1] - 2;
          $dgIndex = (int) $numberToString[2];

          $taskTwoResult = $hd[$hdIndex] . ' ' . $tw[$twIndex] . ' ' . $dg[$dgIndex];
        }
      }
    }

    return $taskTwoResult;
  };

  echo '<h4>Передаём 457:</h4>';
  echo taskTwo(457);

