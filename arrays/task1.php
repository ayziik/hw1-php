<?php
  echo '<h3>Task 1: Найти минимальный элемент массива</h3>';

  function taskOne($someArr) {
    return min($someArr);
  };

  echo '<h4>Передаём [2, -3, -1, 3]:</h4>';
  echo taskOne([2, -3, -1, 3]);