<?php
  echo '<h3>Task 7: Посчитать количество нечетных элементов массива</h3>';

  function taskSeven($someArr) {
    $oddArray = array_filter($someArr, function ($element) {
      return $element % 2 > 0;
    });

    return count($oddArray);
  };

  echo '<h4>Передаём [4, 3, 2, 1]:</h4>';
  echo taskSeven([4, 3, 2, 1]);
