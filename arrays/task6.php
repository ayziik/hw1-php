<?php
  echo '<h3>Task 6: Сделать реверс массива(массив в обратном направлении)</h3>';

  function taskSix($someArr) {
    return array_reverse($someArr);
  };

  echo '<h4>Передаём [4, 3, 2, 1]:</h4>';
  print_r(taskSix([4, 3, 2, 1]));