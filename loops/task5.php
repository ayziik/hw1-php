<?php
  echo '<h3>Task 5: Посчитать сумму цифр заданного числа</h3>';

function taskFive ($number) {
  $stringNumber = (string) $number;
  $taskFiveResult = 0;

  for ($i = 0; $i < strlen($stringNumber); $i++) {
    $taskFiveResult += (int) $stringNumber[$i];
  }

  return $taskFiveResult;
};

  echo '<h4>Передаём 512:</h4>';
  echo taskFive(512);

