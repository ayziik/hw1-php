<?php
  echo '<h3>Task 3: Найти индекс минимального элемента массива</h3>';

  function taskThree($someArr) {
    return array_search(min($someArr), $someArr);
  };

  echo '<h4>Передаём [2, -3, -1, 3]:</h4>';
  echo taskThree([2, -3, -1, 3]);