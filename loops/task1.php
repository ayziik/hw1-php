<?php
echo '<h3>Task 1: Найти сумму четных чисел и их количество в диапазоне от 1 до 99</h3>';

  function taskOne() {
    $evenQuantity = 0;
    $evenSum = 0;

    for ($i = 1; $i <= 99; $i++) {
      if ($i % 2 === 0) {
        $evenQuantity += 1;
        $evenSum += $i;
      }
    }

    return "Количество чётных чисел - $evenQuantity, их сумма - $evenSum";
  };

echo taskOne();

