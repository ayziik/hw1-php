<?php
  echo '<h3>Task 4: Посчитать выражение макс(а*б*с, а+б+с)+3</h3>';

  function taskFour($a, $b, $c){
    if ($a * $b * $c > $a + $b + $c) {
      return $a * $b * $c + 3;
    } else {
      return $a + $b + $c + 3;
    }
  };

  echo '<h4>Передаём a = 2, b = 6, c = 8:</h4>';
  echo taskFour(2, 6, 8);

  echo '<h4>Передаём a = 3, b = 1, c = 1:</h4>';
  echo taskFour(3, 1, 1);