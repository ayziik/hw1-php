<?php
  echo '<h3>Task 9: Отсортировать массив (пузырьком (Bubble), выбором (Select), вставками (Insert))</h3>';

  function taskNineBubble($someArr) {
    for ($i = 0; $i <= count($someArr) - 1; $i++) {
      for ($j = 0; $j < (count($someArr) - 1) - $i; $j++) {
        if($someArr[$j] > $someArr[$j + 1]) {
          $swap = $someArr[$j];
          $someArr[$j] = $someArr[$j + 1];
          $someArr[$j + 1] = $swap;
        }
      }
    }

    return $someArr;
  };

  echo '<h4>Сортировка пузырьком (Bubble), передаём [4, -4, 2, -2]:</h4>';
  print_r(taskNineBubble([4, -4, 2, -2]));

  function taskNineSelect($someArr) {
    for ($i = 0; $i < count($someArr) - 1; $i++) {
      $indexMin = $i;

      for ($j = $i + 1; $j < count($someArr); $j++) {
        if($someArr[$indexMin] > $someArr[$j]) {
          $indexMin = $j;
        }
      }

      $swap = $someArr[$i];
      $someArr[$i] = $someArr[$indexMin];
      $someArr[$indexMin] = $swap;
    }

    return $someArr;
  };

  echo '<h4>Сортировка выбором (Select), передаём [4, -4, 2, -2]:</h4>';
  print_r(taskNineSelect([4, -4, 2, -2]));

  function taskNineInsert($someArr) {
    for ($i = 0; $i < count($someArr); $i++) {
      $current = $someArr[$i];
      $j = $i;

      while ($j > 0 && $someArr[$j - 1] > $current) {
        $someArr[$j] = $someArr[$j - 1];
        $j--;
      }

      $someArr[$j] = $current;
      }

    return $someArr;
  };

  echo '<h4>Сортировка вставками (Insert), передаём [4, -4, 2, -2]:</h4>';
  print_r(taskNineInsert([4, -4, 2, -2]));

