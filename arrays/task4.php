<?php
  echo '<h3>Task 4: Найти индекс максимального элемента массива</h3>';

  function taskFour($someArr) {
    return array_search(max($someArr), $someArr);
  };

  echo '<h4>Передаём [2, -3, -1, 3]:</h4>';
  echo taskFour([2, -3, -1, 3]);
