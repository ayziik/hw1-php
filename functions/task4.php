<?php
  echo '<h3>Task 4: Найти расстояние между двумя точками в двумерном декартовом пространстве</h3>';

  function taskFour($x1, $y1, $x2, $y2) {
    $taskFourResult = sqrt(pow(($x1 - $x2), 2) + pow(($y1 - $y2), 2));

    return round($taskFourResult, 2);
  };

  echo '<h4>Передаём x1 = 6, y1 = 2, x2 = -2, y2 = -3:</h4>';
  echo taskFour(6, 2, -2, -3);