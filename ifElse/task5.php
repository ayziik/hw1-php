<?php
  echo '<h3>Task 5: Написать программу определения оценки студента по его рейтингу</h3>';

  function taskFive($rating) {
    $taskFiveResult = '';

      if ($rating <= 19) {
        $taskFiveResult = 'Оценка студента - F';
      } else if ($rating >= 20 && $rating <= 39) {
        $taskFiveResult = 'Оценка студента - E';
      } else if ($rating >= 40 && $rating <= 59) {
        $taskFiveResult = 'Оценка студента - D';
      } else if ($rating >= 60 && $rating <= 74) {
        $taskFiveResult = 'Оценка студента - C';
      } else if ($rating >= 75 && $rating <= 89) {
        $taskFiveResult = 'Оценка студента - B';
      } else if ($rating >= 90 && $rating <= 100) {
        $taskFiveResult = 'Оценка студента - A';
      } else if ($rating < 0 || $rating > 100) {
        $taskFiveResult = 'Рейтинг студента должен быть в диапазоне от 0 до 100!';
      }

      return $taskFiveResult;
  };

  echo '<h4>Передаём оценку 89:</h4>';
  echo taskFive(89);

  echo '<h4>Передаём оценку 23:</h4>';
  echo taskFive(23);