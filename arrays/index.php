<?php
  echo '<a href="task1.php">Task 1: Найти минимальный элемент массива</a><br>';
  echo '<a href="task2.php">Task 2: Найти максимальный элемент массива</a><br>';
  echo '<a href="task3.php">Task 3: Найти индекс минимального элемента массива</a><br>';
  echo '<a href="task4.php">Task 4: Найти индекс максимального элемента массива</a><br>';
  echo '<a href="task5.php">Task 5: Посчитать сумму элементов массива с нечетными индексами</a><br>';
  echo '<a href="task6.php">Task 6: Сделать реверс массива (массив в обратном направлении)</a><br>';
  echo '<a href="task7.php">Task 7: Посчитать количество нечетных элементов массива</a><br>';
  echo '<a href="task8.php">Task 8: Поменять местами первую и вторую половину массива</a><br>';
  echo '<a href="task9.php">Task 9: Отсортировать массив (пузырьком (Bubble), выбором (Select), вставками (Insert))</a><br>';