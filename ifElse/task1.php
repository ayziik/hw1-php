<?php
  echo '<h3>Task 1: Если а – четное посчитать а*б, иначе а+б</h3>';

  function taskOne($a, $b) {
    if ($a % 2 == 0) {
      return $a * $b;
    }

    return $a + $b;
  };

  echo '<h4>Передаём a = 2, b = 6:</h4>';
  echo taskOne(2, 6);

  echo '<h4>Передаём a = 3, b = 6:</h4>';
  echo taskOne(3, 6);