<?php
  echo '<h3>Task 2: Проверить простое ли число?</h3>';

  function taskTwo($number) {
    $taskTwoResult = '';

    if ($number > 1 && $number !== 2) {
      for ($i = 2; $i < $number; $i++) {
        if ($number % $i == 0) {
          $taskTwoResult = 'Число составное';
          break;
        } else {
          $taskTwoResult = 'Число простое';
        }
      }
    } else if ($number == 2) {
      $taskTwoResult = 'Число простое';
    } else {
      $taskTwoResult = 'Число должно быть больше 1!';
    }

    return $taskTwoResult;
  };

  echo '<h4>Передаём 41:</h4>';
  echo taskTwo(41);

  echo '<h4>Передаём 33:</h4>';
  echo taskTwo(33);