<?php
  echo '<h3>Task 5: Посчитать сумму элементов массива с нечетными индексами</h3>';

  function taskFive($numArray) {
    $taskFiveResult = 0;

    for ($i = 1; $i < count($numArray); $i++) {
      if ($i % 2 > 0) {
        $taskFiveResult += $numArray[$i];
      }
    }

    return $taskFiveResult;
  };

  echo '<h4>Передаём [2, 3, 1, 3]:</h4>';
  echo taskFive([2, 3, 1, 3]);